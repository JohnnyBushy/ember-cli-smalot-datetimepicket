/* jshint node: true */
'use strict';

var path = require('path');

module.exports = {
  name: 'ember-smalot-datetimepicker',

  isDevelopingAddon: function () {
    return true;
  },

  included: function(app) {
    this._super.included(app);

    app.import(path.join(app.bowerDirectory, '/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js'));

    app.import(path.join(app.bowerDirectory, '/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css'));

  }
};
