import Ember from 'ember';

export default Ember.TextField.extend({

  _datetimePicker: null,

  date: null,

  _defaultOptions: {
    format: 'yyyy-mm-dd hh:ii',
    fontAwesome: true,

    disabled: false,
    readonly: true,
    open: false,
    forceDateOutput: false,

    minView: 0,
    maxView: 4,
    weekStart: 1,
    language: "en",

    autoclose: true,
    todayBtn: true,

    startDate: null,
    endDate: null,
    initialDate: null
  },

  options: {},

  _initDatepicker: Ember.on('didInsertElement', function () {
    var $element = this.$(),
      self = this,
      datetimePicker, datetimePickerOptions;

    datetimePickerOptions = Ember.$.extend(this.get("_defaultOptions"), this.get("options"), this.attrs);

    self.set("value", self.get("date"));

    datetimePicker = $element.datetimepicker(datetimePickerOptions);

    this.set("_datetimePicker", datetimePicker);

    datetimePicker.on('changeDate', function (event) {
      var date = $(this).data("datetimepicker").getFormattedDate();
      if (date) {
        self.set("date", date);
      } else {
        self.set("date", null);
      }
    });

  }),

  _destroyDatepicker: Ember.on('willDestroyElement', function () {
    var self = this;

    Ember.run(function () {
      self.get("_datetimePicker").datetimepicker('remove');
    });
  })

});
