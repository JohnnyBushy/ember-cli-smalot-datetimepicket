import SmalotDatetimePicker from "ember-smalot-datetimepicker/components/datetimepicker";

export default SmalotDatetimePicker.extend({
    options: {
        format: 'dd.mm.yyyy',
        minView: 2,
        maxView: 4,
        weekStart: 1,
        language: "ru"
    }

});
